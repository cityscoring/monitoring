https://www.shellhacks.com/prometheus-node-exporter-install-config-ubuntu-centos/

VERSION=0.18.1
RELEASE="${VERSION}.linux"
sudo useradd node_exporter -s /sbin/nologin
wget https://github.com/prometheus/node_exporter/releases/download/v$VERSION/node_exporter-$RELEASE-amd64.tar.gz
tar xvfz node_exporter-$RELEASE-amd64.tar.gz
sudo cp node_exporter-$RELEASE-amd64.tar.gz/node_exporter /usr/sbin/