

== List Server and Roles ===

cluster FR
servers exporter: 
- fr_node1: 188.165.228.139:9100
- fr_node2: 178.33.237.101:9100
elasticsearch exporter:
- fr_cluster: 178.33.237.101:9114

cluster IT
servers exporter: 
- it_node1: 151.80.33.35:9100
elasticsearch exporter:
- it_cluster: 151.80.33.35:9114

cluster ES
servers exporter: 
- es_node1: 151.80.33.19:9100
elasticsearch exporter:
- es_cluster: 151.80.33.19:9114

cluster PT
servers exporter: 
- pt_node1: 88.178.189.115:9100
elasticsearch exporter:
- pt_cluster: 88.178.189.115:9114

- website  
server exporter: 88.178.189.115:9100

- jenkins 
server exporter: 213.32.76.6:9100

- database
server exporter: 94.23.49.36:9100

== SETUP === 

= node_exporter Service =
Follow 
https://www.shellhacks.com/prometheus-node-exporter-install-config-ubuntu-centos/
(ubuntu doesn't have the latest version via apt)

= elasticsearch exporter Service = 

from https://github.com/justwatchcom/elasticsearch_exporter
setup for 1 node of each elasticsearch EACH ElasticSearch Cluster


== UPWORK ==

we want to setup a monitoring+alerting system for a 4 elasticsearch clusters (version 6.8) and 1 website.


We already have a docker compose file integrating latest grafana + prometheus + alert manager + blackbox.  we also already set up the node exporter on each server + /elasticsearch_exporter 


YOUR MISSSION 
From the existing and working docker-compose file, your mission is to create the provisioning files for grafana + prometheus in order to run docker and get a ready-to-use system.


** Milestone 1. Provisionning the datasource **

1. create the Grafana datasources config files
- grafana_provisioning/datasources/es_cluster_fr.yml
- grafana_provisioning/datasources/es_cluster_it.yml
- grafana_provisioning/datasources/es_cluster_es.yml
- grafana_provisioning/datasources/es_cluster_pt.yml
- grafana_provisioning/datasources/promotheus.yml

2. configure promotheus.yml to observe the website + a database server  + each node for each cluster (every 5 minute)

here is the infrastructure 
- 2 nodes/servers for cluster FR 
- 1 node/server for cluster IT
- 1 node/server for cluster ES 
- 1 node/server for cluster PT 
- 1 server for the website
- 1 server for the database

(I will give you the IPs)

for testing purpose you can setup only 1 local Elasticsearch version 6.8.

EXPECTED OUTPUT:
$> docker-compose up  -> prometheus + grafana+ alert manager working. AND if i go to grafana I should see a set of datasources already configured.

PRICE: 50$

** Milestone 2. Provisionning the dashboards (150$) **

creating the dashboards provisionning files
- provisioning/dashboards/es_cluster_fr.yml
- provisioning/dashboards/es_cluster_it.yml
- provisioning/dashboards/es_cluster_es.yml
- provisioning/dashboards/es_cluster_pt.yml
- provisioning/dashboards/website.yml


that should respect the following metrics 

** Dashboard Elasticsearch Cluster (4 cluster = 4 different dashboards) ** 

- Current health status + current shards, current unassigned shards
- evolution of health status
- evolution of unassigned shards
- evolution of total nb docs

for each indice in the cluster
- evolution of nb docs 

for each node/server on the cluster
- Ping (up/down)
- CPU 
- RAM
- JVM memory %.
- Available disk


** Dashboard "Website" **  
- Ping HTTP (up/down) cityscan.iadholding.com
- Ping HTTP (up/down) api.cityobs.com
- [Current CPU] + [CPU evolution ]
- [Current RAM] + [RAM evolution ]
- [Current Available disk ] + [Available disk evolution]
- [Current Network ] + [Network evolution]



(NOTE: you can import existing grafana dashboards as starting point but I dont want dashboard with lots of useless metrics. I want my metrics not more, no less)


EXPECTED OUTPUT:
$> docker-compose up  -> prometheus + grafana+ alert manager working. AND if i go to grafana I should see the dashboard with the metrics. 


** Milestone 3. Alerting system for promotheus (150$) **

- any Node (we should precise the associated cluster) or Website down.
- any Node or Website down for at least 5 minutes
- 95% available disk on any observed server (node or cluster) for at least 2 hours.
- Cluster Health status RED or Yellow for at least 
and "back to normal" alerts for any off the following abnormal status. 
alerts send to alerts@cityscoring.com

EXPECTED OUTPUT:
$> docker-compose up  -> prometheus + grafana+ alert manager working. AND if i go to grafana I should see the dashboard with the metrics. and if I shutdown one of the node , i should receive an email about this info after 5 minutes (node down, cluster red).  if i restart the node , i should received another email saying the cluster is green again.

## Adding more targets to the Grafana dashboard

The dashboard is configured dynamically, which means that the list of servers, URLs, and ElasticSearch clusters is powered by whatever targets Prometheus is scraping.

To add new targets, follow this process:

* Edit the Prometheus config to include the new server, URL, or ElasticSearch cluster. This should be a new Prometheus job, except with a new 
URL, in which case the `blackbox` job should be updated.

For example:
```
  - job_name: 'new-server'
    static_configs:
     - targets: 
        - 100.100.100.100:9100
```
 
 Or for a URL:
```
  - job_name: blackbox
    ...
    static_configs:
     - targets:
       ...
       - <new-url>
```

* Restart Prometheus
* Refresh the Grafana dashboard and ensure the `All` options are selected from the drop down lists at the top
* Dashboards for the new target will be shown

## Configuring AlertManager

To configure AlertManager to push email alerts to your Gmail address, use the following steps.

1. Create a Gmail app password following [these steps](https://support.google.com/accounts/answer/185833?hl=en)
1. In `alert_manager.yml` replace all occurrences of `$GMAIL_ACCOUNT` with your Gmail email address
1. In `alert_manager.yml` replace all occurrences of `$GMAIL_AUTH_TOKEN` with the Gmail app password generated in step 1